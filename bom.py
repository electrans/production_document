# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Or, Not, Bool, If
from trytond.transaction import Transaction
from datetime import timedelta
import datetime
from trytond.i18n import gettext
from trytond.exceptions import UserError


__all__ = ['BOM', 'NewVersionStart', 'NewVersion']


class BOM(metaclass=PoolMeta):
    __name__ = 'production.bom'

    production_master_doc = fields.Many2One(
        'production.document', "Master Document",
        states={
            'invisible': True
            })

    production_doc_edition = fields.Many2One(
        'production.document.edition', "Document Edition",
        domain=[If(Bool(Eval('production_master_doc')),
                   ('master_doc', '=', Eval('production_master_doc')),
                   ())],
        states={
            'readonly': If(Not(Bool(Eval('created_object', False))),
                           False,
                           If(Bool(Eval('used_in_productions', False)),
                              True,
                              Not(Or(Eval('future_version', True),
                                     Eval('active_version', True)))
                              )
                           )},
        depends=['production_master_doc'])

    @classmethod
    def __register__(cls, module_name):
        super(BOM, cls).__register__(module_name)

        # OQA 15/05/2018: this is executed when the table production_document have no data, and it creates rows on this
        # table and doc_edition depending of old char fields: document and  doc_min_edition.
        ProductionDocument = Pool().get('production.document')
        Bom = Pool().get('production.bom')
        if not ProductionDocument.search([], limit=1):
            DocumentEdition = Pool().get('production.document.edition')
            with Transaction().set_context(show_versions=True):
                boms = Bom.search([('document', '!=', '')])
            for bom in boms:
                doc = ProductionDocument.search([('name', '=', bom.document)])
                if not doc:
                    doc = ProductionDocument.create([{'name': bom.document}])
                if bom.doc_min_edition and bom.doc_min_edition.isdigit():
                    ed = DocumentEdition.search([('master_doc', '=', doc[0]), ('edition', '=', int(bom.doc_min_edition))])
                    if not ed:
                        bom_date = bom.create_date.date()
                        ed = DocumentEdition.search([('master_doc', '=', doc[0]), ('start_date', '>=', bom_date),
                                                     ('edition', '<', int(bom.doc_min_edition))])
                        while ed:
                            bom_date += timedelta(days=1)
                            ed = DocumentEdition.search([('master_doc', '=', doc[0]), ('start_date', '>=', bom_date),
                                                         ('edition', '<', int(bom.doc_min_edition))])

                        ed = DocumentEdition.search([('master_doc', '=', doc[0]), ('start_date', '<=', bom_date),
                                                     ('edition', '>', int(bom.doc_min_edition))])
                        while ed:
                            bom_date -= timedelta(days=1)
                            ed = DocumentEdition.search([('master_doc', '=', doc[0]), ('start_date', '<=', bom_date),
                                                         ('edition', '>', int(bom.doc_min_edition))])
                        ed = DocumentEdition.create([{
                            'master_doc': doc[0],
                            'edition': int(bom.doc_min_edition),
                            'start_date': bom_date}])
                    Bom.write([bom], {
                        'production_doc_edition': ed[0],
                        'production_master_doc': doc[0]})

        with Transaction().set_context(show_versions=True):
            boms = Bom.search([('master_bom', '=', None)])
            for bom in boms:
                Bom.write([bom], {
                    'master_bom': bom.id,
                    'start_date': bom.create_date.date(),
                    'version': bom.version_number if bom.version_number else 1})

    @classmethod
    def copy(cls, boms, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        if 'start_date' not in default:
            if not boms[0].start_date or boms[0].start_date <= datetime.date.today():
                default['start_date'] = datetime.date.today() + datetime.timedelta(days=1)
        default['end_date'] = None
        if 'production_doc_edition' not in default:
            default['production_doc_edition'] = None
            default['production_master_doc'] = None
        default['changelog'] = None
        return super(BOM, cls).copy(boms, default=default)

    @fields.depends('production_doc_edition', '_parent_production_doc_edition.master_doc')
    def on_change_with_production_master_doc(self, name=None):
        return (self.production_doc_edition.master_doc.id
            if self.production_doc_edition and self.production_doc_edition.master_doc else None)

    @fields.depends('production_doc_edition')
    def on_change_production_doc_edition(self, name=None):
        if self.production_doc_edition:
            self.changelog = self.production_doc_edition.changelog
        else:
            self.changelog = None


class NewVersionStart(metaclass=PoolMeta):
    __name__ = 'production.bom.new.version.start'

    version = fields.Integer(
        "Version", readonly=True)
    description = fields.Text("Description")
    production_doc = fields.Many2One(
        'production.document.edition', "Document / Edition",
        domain=[If(Bool(Eval('master_doc')),
                   ('master_doc', '=', Eval('master_doc')),
                   ())],
        depends=['master_doc'])
    master_doc = fields.Many2One(
        'production.document', "Document",
        states={
            'invisible': True
            })

    inputs = fields.One2Many(
        'production.bom.input', None, "Inputs")

    @classmethod
    def __setup__(cls):
        super(NewVersionStart, cls).__setup__()
        cls.date.states.update({
            'required': True,
            })

    @staticmethod
    def default_master_doc():
        if Transaction().context.get('active_ids'):
            ProductionBom = Pool().get('production.bom')
            bom = ProductionBom.browse(Transaction().context['active_ids'])
            if bom:
                last_bom = ProductionBom.get_last_version(bom[0].master_bom)
                return last_bom.production_master_doc.id if last_bom and last_bom.production_master_doc else None

    @staticmethod
    def default_version():
        if Transaction().context.get('active_ids'):
            ProductionBom = Pool().get('production.bom')
            bom = ProductionBom.browse(Transaction().context['active_ids'])
            last = ProductionBom.get_last_version(bom[0].master_bom)
            return last.version + 1 if bom and bom[0].master_bom and last else None

    @staticmethod
    def default_date():
        return datetime.date.today() + datetime.timedelta(days=1)

    @staticmethod
    def default_inputs():
        if Transaction().context.get('active_ids'):
            ProductionBom = Pool().get('production.bom')
            bom = ProductionBom.browse(Transaction().context['active_ids'])
            if bom:
                return [input.id for input in bom[0].inputs]

    @fields.depends('production_doc')
    def on_change_with_date(self):
        if self.production_doc:
            return self.production_doc.start_date

    @fields.depends('production_doc')
    def on_change_with_description(self):
        if self.production_doc:
            return self.production_doc.changelog

    @fields.depends('inputs')
    def on_change_with_inputs(self):
        products = []
        for input in self.inputs:
            if input.product in products:
                raise UserError(gettext('electrans.duplicate_input_product'), product=input.product.rec_name)
            products.append(input.product)


class NewVersion(metaclass=PoolMeta):
    __name__ = 'production.bom.new.version'

    def do_create_(self, action):
        ProductionBom = Pool().get('production.bom')
        Input = Pool().get('production.bom.input')
        bom = ProductionBom.browse(Transaction().context.get('active_ids'))
        last_bom = ProductionBom.get_last_version(bom[0].master_bom if bom else None)

        Transaction().context['active_ids'] = [last_bom.id] if last_bom else None
        ProductionBom.write(bom, {'end_date': self.start.date - datetime.timedelta(days=1)})
        with Transaction().set_context(new_version=True):
            new_boms = ProductionBom.copy(bom, {
                'end_date': None,
                'start_date': self.start.date,
                'inputs': None
                })

        master_doc = self.start.production_doc.master_doc if self.start.production_doc else None
        ProductionBom.write(new_boms, {
            'changelog': self.start.description,
            'production_doc_edition': self.start.production_doc,
            'production_master_doc': master_doc})
        inputs = []
        for input in self.start.inputs:
            inputs.append({
                'product': input.product,
                'quantity': input.quantity,
                'uom': input.uom,
                'map_distribution': input.map_distribution,
                'bom': new_boms[0].id})
        Input.create(inputs)

        data = {'res_id': [i.id for i in new_boms]}
        if len(new_boms) == 1:
            action['views'].reverse()

        action, data = super(NewVersion, self).do_create_(action, data)
        return action, data
