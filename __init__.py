# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import doc
from . import bom


def register():
    Pool.register(
        doc.ProductionDocument,
        doc.DocEdition,
        bom.BOM,
        bom.NewVersionStart,
        module='electrans_production_document', type_='model')

    Pool.register(
        doc.CreatePurchase,
        doc.AddToPurchase,
        bom.NewVersion,
        module='electrans_production_document', type_='wizard')