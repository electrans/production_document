# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool, PoolMeta
import datetime
from trytond.i18n import gettext
from trytond.exceptions import UserError


__all__ = ['ProductionDocument', 'DocEdition', 'CreatePurchase', 'AddToPurchase']


class DocEdition(ModelSQL, ModelView):
    "Production Document"
    __name__ = 'production.document.edition'

    edition = fields.Integer(
        "Edition", required=True)
    start_date = fields.Date(
        "Start Date", required=True)
    master_doc = fields.Many2One(
        'production.document', "Document", required=True)
    changelog = fields.Text(
        "Changelog")
    active_edition = fields.Function(fields.Boolean(
        "Active"),
        'get_active_edition')

    @classmethod
    def __setup__(cls):
        super(DocEdition, cls).__setup__()
        cls._order = [('master_doc', 'ASC'), ('edition', 'ASC')]

    def get_active_edition(self, name):
        today = datetime.date.today()
        return True if self.start_date <= today else False

    @classmethod
    def copy(cls, docs, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()

        new_docs = []
        for doc in docs:
            last_doc = cls.get_last_edition(doc.master_doc) if doc.master_doc else []
            if last_doc:
                default['edition'] = last_doc.edition + 1
                if 'start_date' not in default:
                    default['start_date'] = datetime.date.today()
            new_docs.extend(super(DocEdition, cls).copy([doc], default=default))
        return new_docs

    @classmethod
    def create(cls, vlist):
        docs = super(DocEdition, cls).create(vlist)
        for doc in docs:
            doc.check_dates()
            if not doc.edition:
                doc.edition = 1
                doc.save()
        return docs

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        super(DocEdition, cls).write(*args)
        for documents, values in zip(actions, actions):
            for document in documents:
                if values.get('start_date'):
                    document.check_dates()

    @classmethod
    def search_rec_name(cls, name, clause):
        return [('master_doc.rec_name',) + tuple(clause[1:])]

    def get_rec_name(self, name):
        return self.master_doc.name + " - Ed.: " + str(self.edition) if self.master_doc else "Ed.: " + str(self.edition)

    @staticmethod
    def default_start_date():
        return datetime.date.today() + datetime.timedelta(days=1)

    @classmethod
    def get_last_edition(cls, master_doc):
        docs = cls.search([
                ('master_doc', '=', master_doc),
            ], order=[
                ('edition', 'DESC')
            ], limit=1)
        if docs:
            return docs[0]

    @classmethod
    def validate(cls, docs):
        super(DocEdition, cls).validate(docs)
        for doc in docs:
            doc.check_edition()

    def check_dates(self):
        if not self.master_doc:
            return
        domain = [
            ('master_doc', '=', self.master_doc.id),
            ('id', '!=', self.id),
            ['OR',
                [('start_date', '>=', self.start_date), ('edition', '<', self.edition)],
                [('start_date', '<', self.start_date), ('edition', '>', self.edition)]]
            ]
        docs = self.search(domain, limit=1)
        if docs:
            raise UserError(gettext('electrans_production_document.invalid_dates'),
                            doc=self.rec_name,
                            edition=docs[0].edition)

    def check_edition(self):
        if not self.master_doc:
            return
        edition = self.search([('id', '!=', self.id),
                               ('master_doc', '=', self.master_doc),
                               ('edition', '=', self.edition)])
        if edition:
            raise UserError(gettext('electrans_production_document.invalid_edition'))


class ProductionDocument(ModelSQL, ModelView):
    'Production Document'
    __name__ = 'production.document'

    name = fields.Char("Name")
    link = fields.Char("Url")
    document_editions = fields.One2Many(
        'production.document.edition', 'master_doc', "Document Editions")
    ldm_versions = fields.One2Many(
        'production.bom', 'production_master_doc', "LDMs")
    description = fields.Text(
        "Description")

    @classmethod
    def __setup__(cls):
        super(ProductionDocument, cls).__setup__()
        cls._order = [('name', 'ASC')]
        t = cls.__table__()
        cls._sql_constraints += [('unique_name', Unique(t, t.name), 'The name have to be unique.')]

    @classmethod
    def copy(cls, documents, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        new_records = []
        for document in documents:
            default['name'] = 'Copia de ' + document.name
            default['document_editions'] = None
            default['ldm_versions'] = None
            new_records.extend(super(ProductionDocument, cls).copy([document], default=default))
        return new_records


class CreatePurchase(metaclass=PoolMeta):
    __name__ = 'purchase.request.create_purchase'

    @classmethod
    def compute_purchase_line(cls, key, requests, purchase):
        # TODO 4.x compute_purchase_line don't have key param
        line = super(CreatePurchase, cls).compute_purchase_line(
            key, requests, purchase)
        request = requests[0]
        # Complete description of purchase line
        Production = Pool().get('production')
        productions = Production.search([('purchase_request.id', '=', str(request.id))])
        if productions:
            codes = ""
            for p in productions:
                if p.number:
                    codes += p.number + ", "

            f = productions[0]
            delivery_good = f.outputs[0].product if f.outputs else False

            line.description += "\nFabr.: " + codes[:-2]
            if delivery_good:
                line.description += "\nProducto a fabricar: \n%s" % (delivery_good.rec_name)
            if f.subcontract_product:
                if f.bom:
                    line.description += "\nDocumento: " + f.bom.production_doc_edition.rec_name if f.bom.production_doc_edition else ""

        return line


class AddToPurchase(metaclass=PoolMeta):
    __name__ = 'purchase.request.add_to_purchase'

    # TODO: Function mixed from stock_supply.purchase.request.create_request
    @classmethod
    def compute_purchase_line(cls, key, requests, purchase):
        request = requests[0]
        line = super(AddToPurchase, cls).compute_purchase_line(key, requests, purchase)
        Production = Pool().get('production')
        productions = Production.search([('purchase_request.id', '=', str(request.id))])

        if productions:
            codes = ""
            for p in productions:
                if p.number:
                    codes += p.number + ", "

            f = productions[0]
            delivery_good = f.outputs[0].product if f.outputs else False

            line.description += "\nFabr.: " + codes[:-2]
            if delivery_good:
                line.description += "\nProducto a fabricar: \n%s" % (delivery_good.rec_name)
            if f.subcontract_product:
                if f.bom:
                    line.description += "\nDocumento: " + f.bom.production_doc_edition.rec_name if f.bom.production_doc_edition else ""

        return line
